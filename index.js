var jsforce = require('jsforce');
var config = require('./config');

var fieldCounter = 1;

var jsonfile = require('jsonfile');

var createMetadata = function (conn) {
    var metadata = [];

    for (var i = 0; i < 10; i++) {
        metadata.push({  fullName: 'Activity.SampleField_' + fieldCounter + '__c',
        length: 100, 
        type: 'Text',
        label: 'Sample Field ' + fieldCounter});
        fieldCounter ++;
    }
 
    conn.metadata.create('CustomField', metadata, function(err, results) {
        if (err) { 
            console.error(err); 
        } else if (results) {
            for (var i=0; i < results.length; i++) {
                var result = results[i];
                console.log('success ? : ' + result.success);
                console.log('fullName : ' + result.fullName);
            }
        }
    });
}

var retrieveMeta = function (conn, objNames) {
    conn.metadata.read('CustomObject', objNames, function(err, metadata) {
        if (err) { console.error(err); }
        if (metadata) {
            jsonfile.writeFileSync('meta.json', metadata, {spaces: 1});
        }
    });
}

var deleteObjectFields = function (conn) {
    jsonfile.readFile('./meta.json', function(err, obj) {
        var self = this;
        if (obj) {
            self.fieldsToDelete = [];
            for (var i=0; i < obj.fields.length; i++) {
                self.fieldsToDelete.push(obj.fullName + '.' + obj.fields[i].fullName);
            }
            console.log('Fields to delete: %o',self.fieldsToDelete);
            for (var j=0; j < self.fieldsToDelete.length; j++) {
                //setTimeout(function () {
                    console.log('delete:' + self.fieldsToDelete[j]);
                    conn.metadata.delete('CustomField', [fieldsToDelete[j]], function(err, results) {
                        if (err) { console.error(err); }
                        for (var k=0; k < results.length; k++) {
                            var result = results[k];
                            console.log('success ? : ' + result.success);
                            console.log('fullName : ' + result.fullName);
                        }
                    });
                //ß},100);
            }
        }
    })
}

// Connect to SFDC
var conn = new jsforce.Connection();
conn.metadata.pollTimeout = 600*1000; // timeout in 300 sec
conn.login(config.username, config.password).then(function() {
    for (var i=0; i < 10; i++) {
        setTimeout(function () {
            createMetadata(conn);
        }, 100);
    }
    //retrieveMeta(conn,['Activity']);
    //deleteObjectFields(conn);
});